package controllertest

import (
	"bytes"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"net/http/httptest"
	"strconv"
	"testing"

	"github.com/gorilla/mux"
	"gitlab.com/mohammadanang/pollux/api/models"
	"gopkg.in/go-playground/assert.v1"
)

func TestCreateUser(t *testing.T) {
	err := refreshUserTable()
	if err != nil {
		log.Fatal(err)
	}

	samples := []struct {
		inputJSON    string
		statusCode   int
		nickname     string
		email        string
		errorMessage string
	}{
		{
			inputJSON:    `{"nickname": "Fred", "email": "fred@email.com", "password": "password"}`,
			statusCode:   201,
			nickname:     "Fred",
			email:        "fred@email.com",
			errorMessage: "",
		},
		{
			inputJSON:    `{"nickname": "Frank", "email": "fred@email.com", "password": "password"}`,
			statusCode:   500,
			errorMessage: "email already existed",
		},
		{
			inputJSON:    `{"nickname": "Fred", "email": "frank@email.com", "password": "password"}`,
			statusCode:   500,
			errorMessage: "nickname already existed",
		},
		{
			inputJSON:    `{"nickname": "Brod", "email": "brodmail.com", "password": "password"}`,
			statusCode:   422,
			errorMessage: "invalid email",
		},
		{
			inputJSON:    `{"nickname": "", "email": "brod@email.com", "password": "password"}`,
			statusCode:   422,
			errorMessage: "required nickname",
		},
		{
			inputJSON:    `{"nickname": "Brod", "email": "", "password": "password"}`,
			statusCode:   422,
			errorMessage: "required email",
		},
		{
			inputJSON:    `{"nickname": "Brod", "email": "brod@email.com", "password": ""}`,
			statusCode:   422,
			errorMessage: "required password",
		},
	}

	for _, v := range samples {
		req, err := http.NewRequest("POST", "/users", bytes.NewBufferString(v.inputJSON))
		if err != nil {
			t.Errorf("this is the error: %v", err)
		}

		rr := httptest.NewRecorder()
		handler := http.HandlerFunc(server.CreateUser)
		handler.ServeHTTP(rr, req)

		responseMap := make(map[string]interface{})
		err = json.Unmarshal(rr.Body.Bytes(), &responseMap)
		if err != nil {
			fmt.Printf("cannot convert to json: %v", err)
		}

		assert.Equal(t, rr.Code, v.statusCode)
		if v.statusCode == 201 {
			assert.Equal(t, responseMap["nickname"], v.nickname)
			assert.Equal(t, responseMap["email"], v.email)
		}

		if v.statusCode == 422 || v.statusCode == 500 && v.errorMessage != "" {
			assert.Equal(t, responseMap["error"], v.errorMessage)
		}
	}
}

func TestGetUsers(t *testing.T) {
	err := refreshUserTable()
	if err != nil {
		log.Fatal(err)
	}

	_, err = seedUsers()
	if err != nil {
		log.Fatal(err)
	}

	req, err := http.NewRequest("GET", "/users", nil)
	if err != nil {
		t.Errorf("this is the error: %v\n", err)
	}

	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(server.GetUsers)
	handler.ServeHTTP(rr, req)

	var users []models.User
	err = json.Unmarshal(rr.Body.Bytes(), &users)
	if err != nil {
		log.Fatalf("cannot convert to json: %v\n", err)
	}

	assert.Equal(t, rr.Code, http.StatusOK)
	assert.Equal(t, len(users), 2)
}

func TestGetUserByID(t *testing.T) {
	err := refreshUserTable()
	if err != nil {
		log.Fatal(err)
	}

	user, err := seedOneUser()
	if err != nil {
		log.Fatal(err)
	}

	userSample := []struct {
		id           string
		statusCode   int
		nickname     string
		email        string
		errorMessage string
	}{
		{
			id:         strconv.Itoa(int(user.ID)),
			statusCode: 200,
			nickname:   user.Nickname,
			email:      user.Email,
		},
		{
			id:         "unknown",
			statusCode: 400,
		},
	}

	for _, v := range userSample {
		req, err := http.NewRequest("GET", "/users", nil)
		if err != nil {
			t.Errorf("this is the error: %v\n", err)
		}

		req = mux.SetURLVars(req, map[string]string{"id": v.id})
		rr := httptest.NewRecorder()
		handler := http.HandlerFunc(server.GetUser)
		handler.ServeHTTP(rr, req)

		responseMap := make(map[string]interface{})
		err = json.Unmarshal(rr.Body.Bytes(), &responseMap)
		if err != nil {
			log.Fatalf("cannot convert to json: %v", err)
		}

		assert.Equal(t, rr.Code, v.statusCode)
		if v.statusCode == 200 {
			assert.Equal(t, user.Nickname, responseMap["nickname"])
			assert.Equal(t, user.Email, responseMap["email"])
		}
	}
}

func TestUpdateUser(t *testing.T) {
	var AuthEmail, AuthPassword string
	var AuthID uint32
	err := refreshUserTable()
	if err != nil {
		log.Fatal(err)
	}

	users, err := seedUsers()
	if err != nil {
		log.Fatalf("error seeding user: %v\n", err)
	}

	for _, user := range users {
		if user.ID == 2 {
			continue
		}
		AuthID = user.ID
		AuthEmail = user.Email
		AuthPassword = "password"
	}

	token, err := server.SignIn(AuthEmail, AuthPassword)
	if err != nil {
		log.Fatalf("cannot login: %v", token)
	}

	tokenString := fmt.Sprintf("Bearer %v", token)
	samples := []struct {
		id             string
		updateJSON     string
		statusCode     int
		updateNickname string
		updateEmail    string
		tokenGiven     string
		errorMessage   string
	}{
		{
			id:             strconv.Itoa(int(AuthID)),
			updateJSON:     `{"nickname": "Grand", "email": "grand@email.com", "password": "password"}`,
			statusCode:     200,
			updateNickname: "Grand",
			updateEmail:    "grand@email.com",
			tokenGiven:     tokenString,
			errorMessage:   "",
		},
		{
			id:           strconv.Itoa(int(AuthID)),
			updateJSON:   `{"nickname": "Woman", "email": "woman@email.com", "password": ""}`,
			statusCode:   422,
			tokenGiven:   tokenString,
			errorMessage: "required password",
		},
		{
			id:           strconv.Itoa(int(AuthID)),
			updateJSON:   `{"nickname": "Man", "email": "man@email.com", "password": "password"}`,
			statusCode:   401,
			tokenGiven:   "",
			errorMessage: "unauthorized",
		},
		{
			id:           strconv.Itoa(int(AuthID)),
			updateJSON:   `{"nickname": "Woman", "email": "woman@email.com", "password": "password"}`,
			statusCode:   401,
			tokenGiven:   "this is incorrect token",
			errorMessage: "unauthorized",
		},
		{
			id:           strconv.Itoa(int(AuthID)),
			updateJSON:   `{"nickname": "Frank", "email": "kenny.user@email.com", "password": "password"}`,
			statusCode:   500,
			tokenGiven:   tokenString,
			errorMessage: "email already existed",
		},
		{
			id:           strconv.Itoa(int(AuthID)),
			updateJSON:   `{"nickname": "Kenny User", "email": "grand.user@email.com", "password": "password"}`,
			statusCode:   500,
			tokenGiven:   tokenString,
			errorMessage: "nickname already existed",
		},
		{
			id:           strconv.Itoa(int(AuthID)),
			updateJSON:   `{"nickname": "Kens", "email": "kenmail.com", "password": "password"}`,
			statusCode:   422,
			tokenGiven:   tokenString,
			errorMessage: "invalid email",
		},
		{
			id:           strconv.Itoa(int(AuthID)),
			updateJSON:   `{"nickname": "", "email": "kens@email.com", "password": "password"}`,
			statusCode:   422,
			tokenGiven:   tokenString,
			errorMessage: "required nickname",
		},
		{
			id:           strconv.Itoa(int(AuthID)),
			updateJSON:   `{"nickname": "Kevin", "email": "", "password": "password"}`,
			statusCode:   422,
			tokenGiven:   tokenString,
			errorMessage: "required email",
		},
		{
			id:         "unknown",
			tokenGiven: tokenString,
			statusCode: 400,
		},
		{
			id:           strconv.Itoa(int(AuthID)),
			updateJSON:   `{"nickname": "Mike", "email": "mike@email.com", "password": "password"}`,
			statusCode:   401,
			tokenGiven:   tokenString,
			errorMessage: "unauthorized",
		},
	}

	for _, v := range samples {
		req, err := http.NewRequest("POST", "/users", bytes.NewBufferString(v.updateJSON))
		if err != nil {
			t.Errorf("this is the error: %v\n", err)
		}

		req = mux.SetURLVars(req, map[string]string{"id": v.id})
		rr := httptest.NewRecorder()
		handler := http.HandlerFunc(server.UpdateUser)
		req.Header.Set("Authorization", v.tokenGiven)
		handler.ServeHTTP(rr, req)

		responseMap := make(map[string]interface{})
		err = json.Unmarshal(rr.Body.Bytes(), &responseMap)
		if err != nil {
			t.Errorf("cannot convert to json: %v", err)
		}

		assert.Equal(t, rr.Code, v.statusCode)
		if v.statusCode == 200 {
			assert.Equal(t, responseMap["nickname"], v.updateNickname)
			assert.Equal(t, responseMap["email"], v.updateEmail)
		}

		if v.statusCode == 401 || v.statusCode == 422 || v.statusCode == 500 && v.errorMessage != "" {
			assert.Equal(t, responseMap["error"], v.errorMessage)
		}
	}
}

func TestDeleteUser(t *testing.T) {
	var AuthEmail, AuthPassword string
	var AuthID uint32

	err := refreshUserTable()
	if err != nil {
		log.Fatal(err)
	}

	users, err := seedUsers()
	if err != nil {
		log.Fatalf("error seeding user: %v\n", err)
	}

	for _, user := range users {
		if user.ID == 2 {
			continue
		}

		AuthID = user.ID
		AuthEmail = user.Email
		AuthPassword = "password"
	}

	token, err := server.SignIn(AuthEmail, AuthPassword)
	if err != nil {
		log.Fatalf("cannot login: %v\n", err)
	}

	tokenString := fmt.Sprintf("Bearer %v", token)
	userSample := []struct {
		id           string
		tokenGiven   string
		statusCode   int
		errorMessage string
	}{
		{
			id:           strconv.Itoa(int(AuthID)),
			tokenGiven:   tokenString,
			statusCode:   204,
			errorMessage: "",
		},
		{
			id:           strconv.Itoa(int(AuthID)),
			tokenGiven:   "",
			statusCode:   401,
			errorMessage: "unauthorized",
		},
		{
			id:           strconv.Itoa(int(AuthID)),
			tokenGiven:   "this is incorrect token",
			statusCode:   401,
			errorMessage: "unauthorized",
		},
		{
			id:         "unknown",
			tokenGiven: tokenString,
			statusCode: 400,
		},
		{
			id:           strconv.Itoa(int(2)),
			tokenGiven:   tokenString,
			statusCode:   401,
			errorMessage: "unauthorized",
		},
	}

	for _, v := range userSample {
		req, err := http.NewRequest("GET", "/users", nil)
		if err != nil {
			t.Errorf("this is the error; %v\n", err)
		}

		req = mux.SetURLVars(req, map[string]string{"id": v.id})
		rr := httptest.NewRecorder()
		handler := http.HandlerFunc(server.DeleteUser)

		req.Header.Set("Authorization", v.tokenGiven)
		handler.ServeHTTP(rr, req)
		assert.Equal(t, rr.Code, v.statusCode)
		if v.statusCode == 401 && v.errorMessage != "" {
			responseMap := make(map[string]interface{})
			err = json.Unmarshal(rr.Body.Bytes(), &responseMap)
			if err != nil {
				t.Errorf("cannot convert to json: %v", err)
			}

			assert.Equal(t, responseMap["error"], v.errorMessage)
		}
	}
}
