package seed

import (
	"log"

	"github.com/jinzhu/gorm"
	"gitlab.com/mohammadanang/pollux/api/models"
)

var users = []models.User{
	{
		Nickname: "John Doe",
		Email:    "john.doe@email.com",
		Password: "password",
	},
	{
		Nickname: "Jane Doe",
		Email:    "jane_doe@email.com",
		Password: "password",
	},
}

var posts = []models.Post{
	{
		Title:   "Title One",
		Content: "lorem ipsum gredek diet one",
	},
	{
		Title:   "Title Two",
		Content: "dosem pidum wekla rayev",
	},
}

func Load(db *gorm.DB) {
	err := db.Debug().DropTableIfExists(&models.Post{}, &models.User{}).Error
	if err != nil {
		log.Fatalf("cannot drop table: %v", err)
	}

	err = db.Debug().AutoMigrate(&models.User{}, &models.Post{}).Error
	if err != nil {
		log.Fatalf("cannot migrate table: %v", err)
	}

	for i, _ := range users {
		err = db.Debug().Model(&models.User{}).Create(&users[i]).Error
		if err != nil {
			log.Fatalf("cannot seed users table: %v", err)
		}

		posts[i].AuthorID = users[i].ID
		err = db.Debug().Model(&models.Post{}).Create(&posts[i]).Error
		if err != nil {
			log.Fatalf("cannot seed posts table: %v", err)
		}
	}
}
