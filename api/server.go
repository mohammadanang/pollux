package api

import (
	"fmt"
	"log"
	"os"

	"github.com/joho/godotenv"
	"gitlab.com/mohammadanang/pollux/api/controllers"
	"gitlab.com/mohammadanang/pollux/api/seed"
)

var server = controllers.Server{}

func init() {
	// loads value  from .env into the system
	if err := godotenv.Load(); err != nil {
		log.Print(".env value not found")
	}
}

func Run() {
	var err error
	err = godotenv.Load()
	if err != nil {
		log.Fatalf("getting error of env, %v", err)
	} else {
		fmt.Println("getting env values successful")
	}

	server.Initialize(os.Getenv("DB_DRIVER"), os.Getenv("DB_USER"), os.Getenv("DB_PASSWORD"), os.Getenv("DB_PORT"), os.Getenv("DB_HOST"), os.Getenv("DB_NAME"))

	seed.Load(server.DB)

	server.Run(":8080")
}
