package controllers

import (
	"net/http"

	"gitlab.com/mohammadanang/pollux/api/responses"
)

func (server *Server) Home(w http.ResponseWriter, r *http.Request) {
	responses.JSON(w, http.StatusOK, "Welcome to the Pullox API")
}
